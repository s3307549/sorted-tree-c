#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "main.h"
#include "tree.h"

// You are allowed to change anything about this function to fix it
int main() {
    int bufferSize = 20;
    char* commandBuffer = (char*)malloc(sizeof(char) * bufferSize);

    Tree *tree = tree_create();

    for(;;) {
        fgets(commandBuffer, bufferSize, stdin);

        // Quit on EOF or 'q'
        // Note: second condition may trigger if the input starts with 'q'
        //       but that is not a problem for this assignment
        if (feof(stdin) || *commandBuffer == 'q'){
            break;
        }

        int inputTerminates = 0;
        for (int i = 0; i < bufferSize; i++){
            if (commandBuffer[i] == '\0'){
                inputTerminates = 1;
                break;
            }
        }
        if (!inputTerminates){
            fprintf(stderr, "Input too long for buffer\n");
            return 1;
        }

        tree = handleString(commandBuffer, tree);
    }

    tree_delete(tree);
    free(commandBuffer);

    return 0;
}

/**
 * Handle a command entered into the program
 *
 * You are allowed to change anything about this function to fix it
 * @param command The command string to handle
 */
Tree* handleString(char command[], Tree *tree){
    if (command == NULL){
        fprintf(stderr, "Invalid command; null pointer\n");
        return tree;
    }

    switch(command[0]){
        case 'i':
            tree = insert(command, tree);
            break;
        case 'e':
            erase(command, tree);
            break;
        case 'c':
            check(command, tree);
            break;
        case 'p':
            tree_print(tree, 1);
            break;
        case 'x':
            tree_delete(tree);
            return NULL;
        default:
            fprintf(stderr, "Invalid command string: %s\n", command);
            break;
    }

    return tree;
}

int validate_name(const char* name, int length){
    if (name == NULL){
        return 0;
    }
    for (int i = 0; i < length; i++){
        if (!isalpha(name[i])) return 0;

        if (name[i] == '\0') return 1;
    }
    return 0;
}

// You are allowed to change anything about this function to fix it
Tree* insert(char* command, Tree* tree) {
    int age;
    char* name = malloc(sizeof(char) * 20);

    if (2 != sscanf(command, "i %d %19s", &age, name)){
        fprintf(stderr, "Failed to parse insert command: not enough parameters filled\n");
        free(name);
        return tree;
    }

    if (!validate_name(name, 20)){
        fprintf(stderr, "Failed to insert: invalid name\n");
        free(name);
        return tree;
    }

    if (tree == NULL){
        tree = tree_create();
    }

    if (tree_find(tree, age, name)) {
        fprintf(stderr, "Failed to insert: item already exists\n");
        free(name);
        return tree;
    }

    tree_insert(tree, age, name);
    free(name);
    return tree;
}

// You are allowed to change anything about this function to fix it
void erase(char* command, Tree* tree) {
    int age;
    char* name = malloc(sizeof(char) * 20);

    if (2 != sscanf(command, "e %d %19s", &age, name)){
        fprintf(stderr, "Failed to parse erase command: not enough parameters filled\n");
        free(name);
        return;
    }
    tree_erase(tree, age, name);
    free(name);
}

// You are allowed to change anything about this function to fix it
void check(char* command, Tree* tree) {
    int age;
    char* name = malloc(sizeof(char) * 20);

    if (2 != sscanf(command, "c %d %19s", &age, name)){
        fprintf(stderr, "Failed to parse check command\n");
    }

    Node* result = tree_find(tree, age, name);
    free(name);
    if (result){
        printf("y\n");
    } else {
        printf("n\n");
    }
}
