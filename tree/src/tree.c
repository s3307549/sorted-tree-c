#include "tree.h"

/**
 * Please correct the contents of this file to make sure all functions here do what they are supposed to do if you find
 * that they do not work as expected.
 */

// Tree function: you are allowed to change the contents, but not the method signature
Tree* tree_create(){
    Tree *tree = malloc(sizeof(Tree));

    if (tree != NULL)
        tree->root = NULL;

    return tree;
}

// Helper function: you are allowed to change this to your preferences
void tree_node_delete(Node* node) {
    if (node == NULL) return;

    tree_node_delete(node->left);
    tree_node_delete(node->right);

    free(node->name);
    free(node);

}

// Tree function: you are allowed to change the contents, but not the method signature
void tree_delete(Tree* tree) {
    if (tree == NULL) return;
    tree_node_delete(tree->root);

    free(tree);
}

// Helper function: you are allowed to change this to your preferences
void node_insert(Node* node, int age, char* name) {

    int new_node_lesser = 0;
    if (node->age == age){
        new_node_lesser = strcmp(node->name, name) > 0;
    } else {
        new_node_lesser = node->age > age;
    }

    if (new_node_lesser){
        if (node->left == NULL){
            Node* newLeft = malloc(sizeof(Node));
            newLeft->age = age;
            newLeft->name = strdup(name);
            newLeft->left = NULL;
            newLeft->right = NULL;
            node->left = newLeft;
        } else {
            node_insert(node->left, age, name);
        }
    } else {
        if (node->right == NULL){
            Node* newRight = malloc(sizeof(Node));
            newRight->age = age;
            newRight->name = strdup(name);
            newRight->left = NULL;
            newRight->right = NULL;
            node->right = newRight;
        } else {
            node_insert(node->right, age, name);
        }
    }
}

// Tree function: you are allowed to change the contents, but not the method signature
void tree_insert(Tree* tree, int age, char* name) {
    if (tree->root == NULL) {
        Node *node = malloc(sizeof(Node));
        node->name = strdup(name);
        node->age = age;
        node->left = NULL;
        node->right = NULL;
        tree->root = node;
    } else {
        node_insert(tree->root, age, name);
    }
}

Node *tree_min_value_node(Node *node) {
    while (node->left != NULL) {
        node = node->left;
    }

    return node;
}

Node *find_parent(Tree *tree, Node *node) {
    if (tree->root == node) return NULL;

    Node *parent = tree->root;

    while (parent->left != node && parent->right != node) {
        if (node->age <= parent->age) {
            parent = parent->left;
        } else {
            parent = parent->right;
        }
    }

    return parent;
}

// Tree function: you are allowed to change the contents, but not the method signature
void tree_erase(Tree* tree, int age, char* name) {
    Node* data = tree_find(tree, age, name);

    if (data == NULL) return;

    Node *parent = find_parent(tree, data);

    // Leaf node
    if (data->left == NULL && data->right == NULL)
        if (parent == NULL)
            tree->root = NULL;
        else if (parent->left == data)
            parent->left = NULL;
        else
            parent->right = NULL;
    // One child
    else if (data->left == NULL)
        if (parent == NULL)
            tree->root = data->right;
        else if (parent->left == data)
            parent->left = data->right;
        else
            parent->right = data->right;
    // One child (other side)
    else if (data->right == NULL)
        if (parent == NULL)
            tree->root = data->left;
        else if (parent->left == data)
            parent->left = data->left;
        else
            parent->right = data->left;
    // Two children
    else {
        Node *min = tree_min_value_node(data->right);
        Node *min_parent = find_parent(tree, min);

        if (min_parent->left == min)
            min_parent->left = min->right;
        else
            min_parent->right = min->right;

        min->left = data->left;
        min->right = data->right;

        if (parent == NULL) {
            tree->root = min;
        } else if (parent->left == data) {
            parent->left = min;
        } else {
            parent->right = min;
        }
    }

    free(data->name);
    free(data);

}

// Helper function: you are allowed to change this to your preferences
void tree_print_node(Node* node){
    if (node == NULL) {
        printf("null");
        return;
    }

    printf("[");
    printf("{\"%d\":\"%s\"},", node->age, node->name);
    tree_print_node(node->left);
    printf(",");
    tree_print_node(node->right);
    printf("]");
}

// Tree function: you are allowed to change the contents, but not the method signature
void tree_print(Tree* tree, int printNewline){
    if (tree == NULL)
        printf("null");
    else
        tree_print_node(tree->root);

    if (printNewline){
        printf("\n");
    }
}

// Helper function: you are allowed to change this to your preferences
Node* node_find(Node* node, int age, char* name) {
    if (node == NULL) return NULL;

    if (node->age == age && strcmp(node->name, name) == 0) {
        return node;
    }

    if (age <= node->age) {
        return node_find(node->left, age, name);
    } else {
        return node_find(node->right, age, name);
    }
}

// Tree function: you are allowed to change the contents, but not the method signature
Node* tree_find(Tree* tree, int age, char* name) {
    if (tree == NULL) return NULL;
    return node_find(tree->root, age, name);
}
