mkdir "fuzz_test"
mkdir "fuzz_test/input"
mkdir "fuzz_test/input/sample_inputs"
mkdir "fuzz_test/input/files"
mkdir "fuzz_test/output"
cp "main/inc/main.h" "fuzz_test/input/files"
cp "main/src/main.c" "fuzz_test/input/files"
cp "tree/inc/tree.h" "fuzz_test/input/files"
cp "tree/src/tree.c" "fuzz_test/input/files"
cp "main/tests/input_1.txt" "fuzz_test/input/sample_inputs"
cp "main/tests/input_2.txt" "fuzz_test/input/sample_inputs"
cp "main/tests/input_3.txt" "fuzz_test/input/sample_inputs"
export AFL_USE_ASAN=1
afl-clang-lto fuzz_test/input/files/main.c fuzz_test/input/files/tree.c -o fuzz_test/output/fuzzer
afl-fuzz -i fuzz_test/input/sample_inputs/ -o fuzz_test/output/ -V 3600 fuzz_test/output/fuzzer